package sss;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

public class konto extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					konto frame = new konto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public konto() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = 	new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton edytuj_dane = new JButton("edytuj dane");
		edytuj_dane.setBounds(144, 89, 101, 23);
		contentPane.add(edytuj_dane);
		
		JButton usun_konto = new JButton("usu\u0144 konto");
		usun_konto.setBounds(156, 142, 89, 23);
		contentPane.add(usun_konto);
		
		JButton wroc = new JButton("wr\u00F3\u0107");
		wroc.setBounds(335, 212, 89, 23);
		contentPane.add(wroc);
	}

}
