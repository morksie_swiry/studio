package sss;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Studio extends JFrame {
	public static List Listautworów;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Listautworów=new ArrayList();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Studio frame = new Studio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Studio() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblStudioNagra = new JLabel("Studio Nagra\u0144");
		lblStudioNagra.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblStudioNagra.setBounds(125, 25, 176, 34);
		getContentPane().add(lblStudioNagra);
		
		JButton btnNewButton = new JButton("Lisa utwor\u00F3w");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
					Lista  okno = new Lista();
					okno.setVisible(true);
			}
		});
		btnNewButton.setBounds(67, 70, 139, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Sklep");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				sklep okno = new sklep();
				okno.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(212, 70, 139, 23);
		getContentPane().add(btnNewButton_1);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 104, 414, 148);
		getContentPane().add(textArea);
		
		JButton btnWyloguj = new JButton("Wyloguj");
		btnWyloguj.setBounds(335, 11, 89, 12);
		getContentPane().add(btnWyloguj);
		
		JLabel lblNewLabel = new JLabel("User");
		lblNewLabel.setBounds(279, 10, 46, 14);
		getContentPane().add(lblNewLabel);
	}

}
