package sss;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;

public class Lista extends JFrame {

	private JPanel contentPane;
	public static List Listautworów;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Listautworów=new ArrayList();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Lista frame = new Lista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Lista() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 588, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel gatunek = new JLabel("");
		gatunek.setFont(new Font("Tahoma", Font.PLAIN, 14));
		gatunek.setBounds(78, 140, 69, 23);
		contentPane.add(gatunek);
		
		JLabel Album = new JLabel("Albumy");
		Album.setFont(new Font("Tahoma", Font.PLAIN, 14));
		Album.setBounds(50, 174, 58, 23);
		contentPane.add(Album);
		
		JLabel lblNewLabel = new JLabel("Lista Utwor\u00F3w");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 29));
		lblNewLabel.setBounds(195, 11, 195, 37);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Dodaj Utw\u00F3r");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				utwur  okno = new utwur();
				okno.setVisible(true);
			}
		});
		btnNewButton.setBounds(295, 59, 133, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Wyszukaj Utw\u00F3r");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Wyszukiwarka  okno = new Wyszukiwarka();
				okno.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(143, 59, 131, 23);
		contentPane.add(btnNewButton_1);
		
		JLabel rap = new JLabel("Rap");
		rap.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = rap.getText();
				gatunek.setText(gg);
			}
		});
		rap.setBounds(122, 96, 46, 14);
		contentPane.add(rap);
		
		JLabel hip = new JLabel("Hip-Hop");
		hip.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = hip.getText();
				gatunek.setText(gg);
			}
		});
		hip.setBounds(168, 96, 46, 14);
		contentPane.add(hip);
		
		JLabel dis = new JLabel("DiscoPolo");
		dis.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = dis.getText();
				gatunek.setText(gg);
			}
		});
		dis.setBounds(242, 96, 46, 14);
		contentPane.add(dis);
		
		JLabel metal = new JLabel("Metal");
		metal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = metal.getText();
				gatunek.setText(gg);
			}
		});
		metal.setBounds(315, 96, 46, 14);
		contentPane.add(metal);
		
		JLabel jezz = new JLabel("Jezz");
		jezz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = jezz.getText();
				gatunek.setText(gg);
			}
		});
		jezz.setBounds(371, 96, 46, 14);
		contentPane.add(jezz);
		
		JLabel elektro = new JLabel("Elektronik");
		elektro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = elektro.getText();
				gatunek.setText(gg);
			}
		});
		elektro.setBounds(427, 96, 46, 14);
		contentPane.add(elektro);
		
		JLabel techno = new JLabel("Techno");
		techno.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = techno.getText();
				gatunek.setText(gg);
			}
		});
		techno.setBounds(497, 96, 46, 14);
		contentPane.add(techno);
		
		JLabel lblNewLabel_10 = new JLabel("Gatunek :");
		lblNewLabel_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_10.setBounds(10, 140, 69, 23);
		contentPane.add(lblNewLabel_10);
		
		
		JLabel pop = new JLabel("Pop");
		pop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			String gg;
			gg = pop.getText();
			gatunek.setText(gg);
		
		
			}

		});
		pop.setBounds(10, 96, 46, 14);
		contentPane.add(pop);
		
		JLabel rock = new JLabel("Rock");
		rock.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String gg;
				gg = rock.getText();
				gatunek.setText(gg);
			}
		});
		rock.setBounds(66, 96, 46, 14);
		contentPane.add(rock);
	}
}
